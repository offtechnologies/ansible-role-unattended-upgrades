ansible-role-unattended-upgrades
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-unattended-upgrades/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-unattended-upgrades/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Manages unattended-upgrades on Debian systems.

Requirements
------------
None

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: all
  roles:
    - { role: ansible-role-unattended-upgrades }
  vars:
    autoupdate_enabled: true
    autoupdate_blacklist: []
    autoupdate_mail_to: "root"
    autoupdate_mail_on_error: true
```

License
-------

BSD
