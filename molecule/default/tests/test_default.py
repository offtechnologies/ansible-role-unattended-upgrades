import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('svc', [
  'unattended-upgrades'
])
def test_svc(host, svc):
    service = host.service(svc)

    assert service.is_enabled


@pytest.mark.parametrize('file, content', [
  ("/etc/apt/apt.conf.d/10periodic", "APT::Periodic::Unattended-Upgrade"),
  ("/etc/apt/apt.conf.d/50unattended-upgrades", "Unattended-Upgrade::Allowed")
])
def test_files(host, file, content):
    file = host.file(file)

    assert file.exists
    assert file.contains(content)
